# Spring Sample BookABook Application
## Description(current version: 1.0)
This is basic spring server application to show some of of my skills with it.

Application is secured with Spring Boot Security(v_1.00 not including encryption).

Included: Controllers/MVC_config/Repositories/Services

There is also option to change language(Polish/English).

Books and Users are stored in database(MongoDB CE).

Web conntent served in JSP's with help of Apache tails.

## Frameworks & Other
* Java
* Spring Boot, Security
* MongoDB
* Gradle
* Bootstrap
* Apache Tails
## Book list photo of version 1.0
![image](https://user-images.githubusercontent.com/20537194/35756453-4245d584-0863-11e8-8cb2-37a7f0208963.png)
